# Takehome Exercise
Este exercício é para a vaga de DevOps Engineer da Brasil Paralelo, e tem foco
em Virtualização. O objetivo é que mostre facilidade em aprender uma nova
tecnologia ou habilidade nela, proficiência nos conceitos, boas práticas e
organização pessoal. Dê o seu melhor, mas não esperamos que gaste mais de 3-5
horas nesse projeto, após se habituar com as ferramentas utilizadas.

Após ler o Briefing abaixo, escreva o código necessário para realizar a tarefa e
uma descrição de como executá-la localmente (por exemplo, com Minikube) no
README. Utilize Git para separar os commits. Compacte o repositório num .zip, e
entregue por e-mail para konrad@brasilparalelo.com.br.

Esperamos que resolva em 7 dias após o recebimento deste e-mail, mas se precisar
de mais tempo, é só avisar por e-mail.

# Briefing
Na Brasil Paralelo, estamos passando da fase do MVP para um produto que deverá
atender milhões de pessoas. Para tal, precisamos de uma ferramenta que orquestre
múltiplos serviços, ferramentas e sistemas de maneira previsível, escalável e
declarativa.

Neste repositório, há dois serviços demo, `authentication` e `feed`. O primeiro
contém as credenciais cadastradas e os escopos de acesso. O segundo contém
conteúdo, que só pode ser acessado por usuários com o escopo correto. A cada
requisição, `feed` verifica o token fornecido pelo usuário com auth para
permitir ou não acesso. Pode-se testar o funcionamento do sistema colocando os
dois serviços no ar e rodando os comandos abaixo

```
curl -H "Authorization: Bearer 66ec51ac-72ea-479d-8b5f-d99eede929f0" -v localhost:3000/feed/patriota # 200 OK
curl -H "Authorization: Bearer 66ec51ac-72ea-479d-8b5f-d99eede929f0" -v localhost:3000/feed/premium # 403 Forbidden
```

Seu desafio é criar para os serviços um deployment Kubernetes que permita
interagir com eles utilizando um cliente http de fora do cluster, como os
comandos curl acima. Para tal, virtualize cada serviço (usando docker, rkt ou
outros), e use numa receita com um deployer de sua preferência os recursos que
julgar adequados, como services, pods, storages, ingresses, etc.

```
Primeira etapa:
Testar a aplicação local para validar as dependências, versões de pacotes e variáveis. 
Criar as imagens no Docker rrd-clojure e rrd-python
Subir as imagens no Docker para testar aplicação local no curl
```
```
Segunda etapa:
Subir para o projeto para o git
Publicar as imagens no repositório – Usarei DockerHub
docker push rodrigordavila/rrd-clojure:tagname
docker push rodrigordavila/rrd-python:tagname
docker pull rodrigordavila/rrd-clojure:latest
docker pull rodrigordavila/rrd-python:latest
Testar o deploy das imagens
```

```
Terceira etapa:
Construir o cluster kubernets
Testar conectividade
curl -H "Authorization: Bearer 66ec51ac-72ea-479d-8b5f-d99eede929f0" -v http://localhost:3000/feed/patriota # 200 OK
curl -H "Authorization: Bearer 66ec51ac-72ea-479d-8b5f-d99eede929f0" -v localhost:3000/feed/premium # 403 Forbidden
```

####################Imagem Node#########################
```
FROM node

ENV PROJECT_DIR /usr/local/src/webapp/
WORKDIR ${PROJECT_DIR}
COPY package.json ${PROJECT_DIR}
RUN apt-get -y update
RUN apt-get -y install default-jre
RUN java -version
RUN npm install
RUN npm install -g shadow-cljs
RUN npm install xregexp concat-stream content-type cookies lru nodemon random-bytes xhr2 xmlhttprequest xregexp
COPY . ${PROJECT_DIR}
RUN npm run release

CMD [ "npm", "run", "start", "--base-href=$USERINFO_URL"]

EXPOSE 3000
```
#######################Imagem Python#####################
```
FROM python:3.7

ENV PROJECT_DIR /usr/local/src/webapp/
WORKDIR ${PROJECT_DIR}
COPY Pipfile ${PROJECT_DIR}
COPY . ${PROJECT_DIR}
RUN pip install pipenv
RUN pipenv install 

CMD ["pipenv", "run", "python", "src/server.py"]

EXPOSE 8037
```
################Docker###################
```
docker build -t rrd-clojure .
docker build -t rrd-python .

docker tag rrd-python rodrigordavila/rrd-python
docker push rodrigordavila/rrd-python:latest
docker pull rodrigordavila/rrd-python

docker tag rrd-clojure rodrigordavila/rrd-clojure
docker push rodrigordavila/rrd-clojure:latest
docker pull rodrigordavila/rrd-python:latest
```
##################Kubernets####################
```
minikube start --cpus 2 --memory 2048
kubectl get pods
kubectl get service node
kubectl get deployments

kubectl apply -f appclojure.yaml
kubectl apply -f apppython.yaml
kubectl apply -f service-clojure.yaml
kubectl apply -f service-python.yaml

kubectl delete -f appclojure.yaml
kubectl delete -f apppython.yaml
kubectl delete -f service-python.yaml
kubectl delete -f service-clojure.yaml
```
